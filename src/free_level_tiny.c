/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_level_tiny.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/16 12:43:43 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** STATIC FUNCTIONS ************************************************************
********************************************************************************
*/

static uint8_t		free_arena(t_m_arena *arena, t_m_arena *arena_prev)
{
	if (((t_m_piece*)((uint8_t*)arena + sizeof(t_m_arena)))->length
	== arena->length - sizeof(t_m_piece))
	{
		if (arena != g_malloc.tiny)
		{
			arena_prev->next = arena->next;
			munmap(arena, arena->length + sizeof(t_m_arena));
			return (0);
		}
		else if (arena->next)
		{
			g_malloc.tiny = arena->next;
			munmap(arena, arena->length + sizeof(t_m_arena));
			return (0);
		}
	}
	return (1);
}

static t_m_piece	*free_piece(t_m_arena *arena, t_m_piece *piece,
								t_m_piece *prev)
{
	t_m_piece		*to_free;
	t_m_piece		*next;

	if (prev && prev->free)
	{
		to_free = prev;
		to_free->length += piece->length + sizeof(t_m_piece);
	}
	else
	{
		to_free = piece;
		to_free->free = 1;
	}
	next = (t_m_piece*)((uint8_t*)piece + piece->length + sizeof(t_m_piece));
	if ((uint8_t*)next < (uint8_t*)arena + sizeof(t_m_arena) + arena->length
	&& next->free)
		to_free->length += next->length + sizeof(t_m_piece);
	return (to_free);
}

static void			free_pieces(t_m_arena *arena)
{
	uint8_t			*memory;
	size_t			i;
	t_m_piece		*piece;
	t_m_piece		*piece_prev;

	piece_prev = NULL;
	memory = (uint8_t*)arena + sizeof(t_m_arena);
	i = 0;
	while (1)
	{
		piece = (t_m_piece*)(memory + i);
		if (!piece->free && piece->level == g_malloc.level)
		{
			piece = free_piece(arena, piece, piece_prev);
			i = (uint8_t*)piece - memory;
		}
		i += piece->length + sizeof(t_m_piece);
		if (i >= arena->length)
			break ;
		piece_prev = piece;
	}
}

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void				malloc_free_level_tiny(void)
{
	t_m_arena		*arena;
	t_m_arena		*arena_prev;
	t_m_arena		*arena_next;

	arena = g_malloc.tiny;
	arena_prev = NULL;
	while (arena)
	{
		arena_next = arena->next;
		free_pieces(arena);
		if (!free_arena(arena, arena_prev))
		{
			arena = arena_next;
			continue ;
		}
		arena_prev = arena;
		arena = arena->next;
	}
}

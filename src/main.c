/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/02/02 10:40:51 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include <stdio.h>

/*
********************************************************************************
** STATIC FUNCTIONS ************************************************************
********************************************************************************
*/

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

int						main(void)
{
	void				*ptr[100];

	/*
	******************************	
	** MALLOC ********************
	******************************	
	*/
	ct_putendl("----- MALLOC");
	ptr[0] = malloc(5);
	ptr[1] = malloc(10);
	ptr[2] = malloc(15);
	show_alloc_mem();
	/*
	******************************	
	** FREE **********************
	******************************	
	*/
	ct_putendl("\n----- FREE");
	free(ptr[0]);
	free(ptr[1]);
	free(ptr[2]);
	show_alloc_mem();
	/*
	******************************	
	** MALLOC FREE SPACE *********
	******************************	
	*/
	ct_putendl("\n----- MALLOC FREE SPACE");
	ptr[0] = malloc(10);
	ptr[1] = malloc(15);
	free(ptr[0]);
	show_alloc_mem();
	ptr[0] = malloc(10);
	show_alloc_mem();
	/*
	******************************	
	** MALLOC NO SPACE BEFORE ****
	******************************	
	*/
	ct_putendl("\n----- MALLOC NO SPACE BEFORE");
	free(ptr[0]);
	show_alloc_mem();
	ptr[0] = malloc(30);
	show_alloc_mem();
	free(ptr[1]);
	free(ptr[0]);
	/*
	******************************	
	** FREE DEFRAGMENTATION ******
	******************************	
	*/
	ct_putendl("\n----- FREE DEFRAGMENTATION 1");
	ptr[0] = malloc(10);
	ptr[1] = malloc(10);
	ptr[2] = malloc(10);
	show_alloc_mem();
	free(ptr[0]);
	show_alloc_mem();
	free(ptr[1]);
	show_alloc_mem();
	ct_putendl("\n----- FREE DEFRAGMENTATION 2");
	ptr[0] = malloc(15);
	show_alloc_mem();
	free(ptr[2]);
	show_alloc_mem();
	free(ptr[0]);
	ct_putendl("\n----- FREE DEFRAGMENTATION 3");
	ptr[0] = malloc(10);
	ptr[1] = malloc(10);
	show_alloc_mem();
	free(ptr[1]);
	show_alloc_mem();
	free(ptr[0]);
	show_alloc_mem();
	ct_putendl("\n----- FREE DEFRAGMENTATION 4 (LIMIT RIGHT)");
	ptr[0] = malloc(91);
	show_alloc_mem();
	free(ptr[0]);
	show_alloc_mem();
	ct_putendl("\n----- FREE DEFRAGMENTATION 5 (LIMIT RIGHT)");
	ptr[0] = malloc(96);
	show_alloc_mem();
	free(ptr[0]);
	show_alloc_mem();
	/*
	******************************	
	** REALLOC *******************
	******************************	
	*/
	ct_putendl("\n----- REALLOC UP SPACE");
	ptr[0] = malloc(10);
	show_alloc_mem();
	ptr[0] = realloc(ptr[0], 15);
	show_alloc_mem();
	ct_putendl("\n----- REALLOC UP NO SPACE");
	ptr[1] = malloc(5);
	show_alloc_mem();
	ptr[0] = realloc(ptr[0], 20);
	show_alloc_mem();
	free(ptr[0]);
	free(ptr[1]);
	ct_putendl("\n----- REALLOC DOWN");
	ptr[0] = malloc(10);
	show_alloc_mem();
	ptr[0] = realloc(ptr[0], 8);
	show_alloc_mem();
	ct_putendl("\n----- REALLOC DOWN STRUCT SPACE");
	ptr[1] = malloc(10);
	show_alloc_mem();
	ptr[0] = realloc(ptr[0], 2);
	show_alloc_mem();
	ct_putendl("\n----- REALLOC DOWN NO STRUCT SPACE");
	ptr[0] = realloc(ptr[0], 8);
	show_alloc_mem();
	ptr[0] = realloc(ptr[0], 4);
	show_alloc_mem();
	ct_putendl("\n----- REALLOC TO BIG");
	free(ptr[1]);
	show_alloc_mem();
	ptr[0] = realloc(ptr[0], 150);
	show_alloc_mem();
	free(ptr[0]);
	show_alloc_mem();
	ct_putendl("\n----- REALLOC LARGE");
	ptr[0] = malloc(6000);
	show_alloc_mem();
	ptr[0] = realloc(ptr[0], 7000);
	show_alloc_mem();
	ct_putendl("\n----- REALLOC LARGE TO SMALL");
	ptr[0] = realloc(ptr[0], 4000);
	show_alloc_mem();
	free(ptr[0]);
	show_alloc_mem();

	ct_putendl("\n----- LEVEL TEST 1");
	ptr[0] = malloc(15);
	ptr[1] = malloc(15);
	alloc_level_up();
	ptr[2] = malloc(15);
	ptr[3] = malloc(15);
	alloc_level_up();
	ptr[4] = malloc(15);
	ptr[5] = malloc(15);
	ptr[6] = malloc(15);
	show_alloc_mem();
	alloc_level_save(ptr[6]);
	show_alloc_mem();
	alloc_level_down();
	show_alloc_mem();
	alloc_level_down();
	show_alloc_mem();
	free(ptr[0]);
	free(ptr[1]);
	ct_putendl("\n----- LEVEL TEST 2");
	alloc_level_up();
	ptr[0] = malloc(15);
	ptr[1] = malloc(15);
	ptr[2] = malloc(15);
	ptr[3] = malloc(15);
	ptr[4] = malloc(15);
	show_alloc_mem();
	alloc_level_down();
	show_alloc_mem();
	ct_putendl("\n----- LEVEL TEST 3");
	ptr[0] = malloc(16000);
	alloc_level_up();
	ptr[1] = malloc(16000);
	ptr[2] = malloc(16000);
	ptr[3] = malloc(16000);
	show_alloc_mem();
	alloc_level_down();
	show_alloc_mem();
	ct_putendl("\n----- LEVEL TEST 4");
	free(ptr[0]);
	alloc_level_up();
	ptr[0] = malloc(16000);
	ptr[1] = malloc(16000);
	ptr[2] = malloc(16000);
	show_alloc_mem();
	alloc_level_down();
	show_alloc_mem();
	ct_putendl("\n----- LEVEL TEST 5");
	alloc_level_up();
	ptr[0] = malloc(16000);
	ptr[1] = malloc(16000);
	ptr[2] = malloc(16000);
	show_alloc_mem();
	alloc_level_save(ptr[2]);
	show_alloc_mem();
	alloc_level_down();
	show_alloc_mem();
	free(ptr[2]);

	ct_putendl("\n----- SHOW PIECE");
	ptr[0] = malloc(300);
	ct_memcpy(ptr[0], "coucou comment vas tu ?", 23);
	show_alloc_mem_slot(ptr[0]);
	free(ptr[0]);

	ct_putendl("\n----- CRASH FREE");
	free((void *)-1);
	free((void *)314);
	free((void *)3141592653);
	ct_putendl("\n----- CRASH REALLOC");
	ptr[0] = realloc((void *)-1, 5);
	ptr[1] = realloc((void *)314, 5);
	ptr[2] = realloc((void *)3141592653, 5);

	ct_putendl("\n----- MALLOC MEMORY BUG ??");
	ptr[0] = malloc(64);
	ct_memcpy(ptr[0], "0123456789abcdefghijklmnopqrstuvwxyz", 36);
	show_alloc_mem_slot(ptr[0]);
	show_alloc_mem();
	free(ptr[0]);
	ptr[0] = malloc(64);
	show_alloc_mem_slot(ptr[0]);
	show_alloc_mem();


	//((t_m_piece*)(g_malloc.tiny))->length = -1;
	//((t_m_piece*)(g_malloc.tiny))->level = -1;
	//printf("\n\n%u\n", ((t_m_piece*)(g_malloc.tiny))->length);
	//printf("%hhu\n", ((t_m_piece*)(g_malloc.tiny))->level);
	//printf("SIZE %zu\n", sizeof(t_m_piece));
	return (0);
}

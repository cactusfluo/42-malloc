/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_tiny.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/29 14:58:40 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void		malloc_free_piece(t_m_arena *arena, t_m_piece *piece,
								t_m_piece *prev)
{
	t_m_piece	*to_free;
	t_m_piece	*next;

	if (prev && prev->free)
	{
		to_free = prev;
		to_free->length += piece->length + sizeof(t_m_piece);
	}
	else
	{
		to_free = piece;
		to_free->free = 1;
	}
	next = (t_m_piece*)((uint8_t*)piece + piece->length + sizeof(t_m_piece));
	if ((uint8_t*)next < (uint8_t*)arena + sizeof(t_m_arena) + arena->length
	&& next->free)
		to_free->length += next->length + sizeof(t_m_piece);
}

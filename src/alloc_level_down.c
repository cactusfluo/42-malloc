/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_level_down.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/12 11:12:46 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

uint8_t		alloc_level_down(void)
{
	if (!g_malloc.level)
		return (1);
	if (g_malloc.tiny)
		malloc_free_level_tiny();
	if (g_malloc.small)
		malloc_free_level_small();
	if (g_malloc.large)
		malloc_free_level_large();
	g_malloc.level--;
	return (0);
}

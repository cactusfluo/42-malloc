/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_tiny_small.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/02/02 10:38:45 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** STATIC FUNCTIONS ************************************************************
********************************************************************************
*/

static void			puthex(uint8_t n)
{
	static char		*hex = "0123456789abcdef";

	ct_putchar(hex[(n / 16) % 16]);
	ct_putchar(hex[n % 16]);
	ct_putchar(' ');
}

static void			putplace(size_t n)
{
	static char		*hex = "0123456789abcdef";
	uint32_t		pow;

	pow = 16777216;
	while (pow != 0)
	{
		ct_putchar(hex[(n / pow) % 16]);
		pow /= 16;
	}
	ct_putstr("  ");
}

static void			show_piece(t_m_piece *piece)
{
	char			string[17];
	size_t			length;
	size_t			i;
	uint8_t			*memory;
	int8_t			j;

	length = piece->length;
	memory = (uint8_t*)piece + sizeof(t_m_piece);
	i = 0;
	while (i < length)
	{
		putplace(i);
		j = -1;
		while (++j < 16 && i + j < length)
		{
			puthex(memory[i + j]);
			string[j] = (ct_isprint(memory[i + j])) ? memory[i + j] : '.';
		}
		string[j] = 0;
		while (j++ < 16)
			ct_putstr("   ");
		ct_putchar(' ');
		ct_putendl(string);
		i += 16;
	}
}

static uint8_t		check_piece(t_m_arena *arena, uint8_t *ptr)
{
	uint8_t			*memory;
	size_t			i;
	t_m_piece		*piece;

	memory = (uint8_t*)arena + sizeof(t_m_arena);
	i = 0;
	while (1)
	{
		piece = (t_m_piece*)(memory + i);
		if (ptr == (uint8_t*)piece + sizeof(t_m_piece))
		{
			show_piece(piece);
			return (0);
		}
		i += piece->length + sizeof(t_m_piece);
		if (i >= arena->length || ptr < (uint8_t*)piece)
			break ;
	}
	return (1);
}

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

uint8_t				malloc_show_slot_tiny_small(t_m_arena *arena, uint8_t *ptr)
{
	uint8_t			*memory;
	t_m_arena		*arena_prev;

	arena_prev = NULL;
	while (arena)
	{
		memory = (uint8_t*)arena + sizeof(t_m_arena);
		if (ptr >= memory && ptr < memory + arena->length)
		{
			check_piece(arena, ptr);
			return (0);
		}
		arena_prev = arena;
		arena = arena->next;
	}
	return (1);
}

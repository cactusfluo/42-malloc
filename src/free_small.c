/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_small.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/29 10:52:10 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** STATIC FUNCTIONS ************************************************************
********************************************************************************
*/

static void		free_arena(t_m_arena *arena, t_m_arena *arena_prev)
{
	if (((t_m_piece*)((uint8_t*)arena + sizeof(t_m_arena)))->length
	== arena->length - sizeof(t_m_piece))
	{
		if (arena_prev)
		{
			arena_prev->next = arena->next;
			munmap(arena, arena->length + sizeof(t_m_arena));
		}
		else
		{
			g_malloc.small = arena->next;
			munmap(arena, arena->length + sizeof(t_m_arena));
		}
	}
}

static void		free_piece(t_m_arena *arena, t_m_piece *piece, t_m_piece *prev)
{
	t_m_piece	*to_free;
	t_m_piece	*next;

	if (prev && prev->free)
	{
		to_free = prev;
		to_free->length += piece->length + sizeof(t_m_piece);
	}
	else
	{
		to_free = piece;
		to_free->free = 1;
	}
	next = (t_m_piece*)((uint8_t*)piece + piece->length + sizeof(t_m_piece));
	if ((uint8_t*)next < (uint8_t*)arena + sizeof(t_m_arena) + arena->length
	&& next->free)
		to_free->length += next->length + sizeof(t_m_piece);
}

static uint8_t	check_piece(t_m_arena *arena, uint8_t *ptr)
{
	uint8_t		*memory;
	size_t		i;
	t_m_piece	*piece;
	t_m_piece	*piece_prev;

	piece_prev = NULL;
	memory = (uint8_t*)arena + sizeof(t_m_arena);
	i = 0;
	while (1)
	{
		piece = (t_m_piece*)(memory + i);
		if (ptr == (uint8_t*)piece + sizeof(t_m_piece))
		{
			free_piece(arena, piece, piece_prev);
			return (0);
		}
		i += piece->length + sizeof(t_m_piece);
		if (i >= arena->length || ptr < (uint8_t*)piece)
			break ;
		piece_prev = piece;
	}
	return (1);
}

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

uint8_t			malloc_free_small(uint8_t *ptr)
{
	uint8_t		*memory;
	t_m_arena	*arena;
	t_m_arena	*arena_prev;

	arena_prev = NULL;
	arena = g_malloc.small;
	while (arena)
	{
		memory = (uint8_t*)arena + sizeof(t_m_arena);
		if (ptr >= memory && ptr < memory + arena->length)
		{
			if (!check_piece(arena, ptr))
				free_arena(arena, arena_prev);
			return (0);
		}
		arena_prev = arena;
		arena = arena->next;
	}
	return (1);
}

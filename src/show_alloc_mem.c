/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/29 10:52:11 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** STATIC FUNCTIONS ************************************************************
********************************************************************************
*/

static void			show_slot(void *start, size_t size, uint8_t level)
{
	ct_putchar('[');
	ct_putuint8(level);
	ct_putstr("] ");
	ct_putaddr((uintptr_t)start);
	ct_putstr(" - ");
	ct_putaddr((uintptr_t)start + (uintptr_t)size);
	ct_putstr(" : ");
	ct_putsize(size);
	ct_putstr(" octet");
	if (size > 1)
		ct_putchar('s');
	ct_putchar('\n');
}

static void			print_arena_info(t_m_arena *arena, char *type)
{
	ct_putstr(type);
	ct_putstr(" : ");
	ct_putaddr((uintptr_t)((uint8_t*)arena + sizeof(t_m_arena)));
	ct_putchar('\n');
}

static size_t		show_arenas(t_m_arena *arena, char *type)
{
	size_t			total;
	t_m_piece		*piece;
	size_t			i;
	uint8_t			*memory;

	total = 0;
	while (arena)
	{
		print_arena_info(arena, type);
		memory = (uint8_t*)arena + sizeof(t_m_arena);
		i = 0;
		while (1)
		{
			if (!(piece = (t_m_piece*)(memory + i))->free)
			{
				show_slot((void*)((uint8_t*)piece + sizeof(t_m_piece)),
							piece->length, piece->level);
				total += piece->length;
			}
			if ((i += piece->length + sizeof(t_m_piece)) >= arena->length)
				break ;
		}
		arena = arena->next;
	}
	return (total);
}

static	size_t		show_large(void)
{
	t_m_piece_l		*piece;
	size_t			total;

	total = 0;
	piece = g_malloc.large;
	while (piece)
	{
		show_slot((void*)((uint8_t*)piece + sizeof(t_m_piece_l)),
					piece->length, piece->level);
		total += piece->length;
		piece = piece->next;
	}
	return (total);
}

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void				show_alloc_mem(void)
{
	size_t			total;

	pthread_mutex_lock(&g_malloc_lock);
	total = 0;
	if (g_malloc.tiny)
		total += show_arenas(g_malloc.tiny, "TINY");
	if (g_malloc.small)
		total += show_arenas(g_malloc.small, "SMALL");
	if (g_malloc.large)
	{
		ct_putstr("LARGE:\n");
		total += show_large();
	}
	if (g_malloc.tiny || g_malloc.small || g_malloc.large)
	{
		ct_putstr("Total : ");
		ct_putsize(total);
		ct_putstr(" octet");
		if (total > 1)
			ct_putchar('s');
		ct_putchar('\n');
	}
	else
		ct_putstr("NO MEMORY ALLOCATED\n");
	pthread_mutex_unlock(&g_malloc_lock);
}

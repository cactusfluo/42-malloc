/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:37:43 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/30 14:01:51 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H

/*
********************************************************************************
** INCLUDES ********************************************************************
********************************************************************************
*/

# include <sys/mman.h>
# include <stddef.h>
# include <stdint.h>
# include <pthread.h>
# include "full_libct.h"

/*
********************************************************************************
** DEFINES *********************************************************************
********************************************************************************
*/

/*
** MAC-OS MALLOC: (measures in bytes)
**
**	32-bit:
**		TINY: 	    1 -   496
**		SMALL:	  497 - 15359
**		LARGE:	15360 -
**	64-bit:
**		TINY: 	     1 -    992
**		SMALL:	   993 - 130047
**		LARGE:	130048 -
**
** PAGE SIZE -> 4096 bytes (4KB)
**	32-bit:
**		TINY:	 256 pages -> 1MB	[1048576]
**		SMALL:	4096 pages -> 8MB	[8388608]
**	64-bit:
**		TINY:	 512 pages -> 2MB	[2097152]
**		SMALL:	8192 pages -> 16MB	[16777216]
*/

# define MALLOC_TINY			496
# define MALLOC_SMALL			15359
# define MALLOC_ARENA_TINY		256 * getpagesize()
# define MALLOC_ARENA_SMALL		4096 * getpagesize()
# define MALLOC_LEVEL_MAX		127

/*
********************************************************************************
** TYPEDEFS ********************************************************************
********************************************************************************
*/

typedef	struct s_malloc_piece		t_m_piece;
typedef	struct s_malloc_piece_large	t_m_piece_l;
typedef	struct s_malloc_arena		t_m_arena;
typedef	struct s_malloc				t_malloc;

/*
********************************************************************************
** STRUCTURES ******************************************************************
********************************************************************************
*/

struct	s_malloc_piece
{
	uint32_t		length : 24;
	uint8_t			level : 7;
	uint8_t			free : 1;
};

struct	s_malloc_piece_large
{
	size_t			length;
	t_m_piece_l		*next;
	uint8_t			level : 7;
};

struct	s_malloc_arena
{
	size_t			length;
	t_m_arena		*next;
};

struct	s_malloc
{
	t_m_arena		*tiny;
	t_m_arena		*small;
	t_m_piece_l		*large;
	uint8_t			level : 7;
	uint8_t			init : 1;
};

/*
********************************************************************************
** GLOBALS *********************************************************************
********************************************************************************
*/

extern t_malloc			g_malloc;
extern pthread_mutex_t	g_malloc_lock;

/*
********************************************************************************
** FUNCTIONS *******************************************************************
********************************************************************************
*/

/*
**************************************************
** LOCAL FUNCTIONS *******************************
**************************************************
*/

uint8_t	malloc_init(void);
void	*malloc_alloc(size_t size);
void	*malloc_alloc_tiny(size_t size);
void	*malloc_alloc_small(size_t size);
uint8_t	malloc_free_tiny(uint8_t *ptr);
uint8_t	malloc_free_small(uint8_t *ptr);
void	malloc_free_large(uint8_t *ptr);
uint8_t	*malloc_realloc_tiny_small(t_m_arena *arena, uint8_t *ptr, size_t size);
uint8_t	*malloc_realloc_large(uint8_t *ptr, size_t size);
uint8_t	*malloc_reallocf_tiny_small(t_m_arena *arena, uint8_t *ptr,
									size_t size);
uint8_t	*malloc_reallocf_large(uint8_t *ptr, size_t size);
void	malloc_free_level_tiny(void);
void	malloc_free_level_small(void);
void	malloc_free_level_large(void);
void	malloc_free_piece(t_m_arena *arena, t_m_piece *piece, t_m_piece *prev);
uint8_t	malloc_show_slot_tiny_small(t_m_arena *arena, uint8_t *ptr);
void	malloc_show_slot_large(uint8_t *ptr);

/*
**************************************************
** PUBLIC FUNCTIONS ******************************
**************************************************
*/

void	*malloc(size_t size);
void	*calloc(size_t count, size_t size);
void	*realloc(void *ptr, size_t size);
void	*reallocf(void *ptr, size_t size);
void	free(void *ptr);
void	show_alloc_mem(void);
void	show_alloc_mem_slot(void *ptr);
uint8_t	alloc_level_up(void);
uint8_t	alloc_level_down(void);
void	alloc_level_save(void *ptr);

#endif

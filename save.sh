#!/bin/sh

C_RED="\033[31;01m"
C_GREEN="\033[32;01m"
C_YELLOW="\033[33;01m"
C_BLUE="\033[34;01m"
C_PINK="\033[35;01m"
C_CYAN="\033[36;01m"
C_NO="\033[0m"

#######################
 ### FLAGS + USAGE ###
#######################

usage()
{
	echo -e "${C_RED}$1"
	echo -e "${C_YELLOW}Usage:${C_BLUE} ./save [-m] message"
	echo -e "${C_YELLOW}Help:${C_BLUE} Save the project with git"
	echo -e "\t- git add"
	echo -e "\t- git commit \"message\""
	echo -e "\t- git push"
	echo -e "${C_YELLOW}Options:"
	echo -e "${C_GREEN}\t-m\t${C_NO}launch \"make fclean\" before \"git add\""
	exit 1
}

# flags
f_make=0

# function that set $char to the correspondant character in $1
getchar()
{
	char=`echo $1 | head -c $2 | tail -c 1`
}

# loop through arguments
#  to get the flags
for i in "$@"; do
	getchar "${i}" 1
	if [ $char != "-" ]; then
		break;
	else
		j=2
		getchar ${i} $j
		while [ $char ];do
			if [ $char = "m" ]; then
				f_make=1
			else
				usage "Invalid flag"
			fi
			j=$(($j + 1))
			getchar ${i} $j
		done
	fi
done

message=$i

##############
 ### MAIN ###
###############

success()
{
	echo -e "${C_CYAN}[${C_YELLOW}OK${C_CYAN}]${C_PINK} - ${C_GREEN}$1$C_NO"
}

failure()
{
	echo -e "${C_RED}[${C_YELLOW}FAIL${C_RED}]${C_PINK} - ${C_RED}$1$C_NO"
}

# MAKE FCLEAN
if [ $f_make -eq 1 ]; then
	make fclean
	success "CLEANED"
fi
if [ $? -ne 0 ]; then
	failure "CANNOT FCLEAN WITH MAKEFILE"
	exit 1
fi

# GIT ADD
git add -A
if [ $? -ne 0 ]; then
	failure "CANNOT ADD"
	exit 1
fi
success	"ADDED EVERYTHING"

# GIT COMMIT
git commit -m "$message"
if [ $? -ne 0 ]; then
	failure "CANNOT COMMIT"
	exit 1
fi
success "COMMITED"
echo -e "\"${C_PINK}$message${C_NO}\""

# GIT PUSH
git push
if [ $? -ne 0 ]; then
	failure "CANNOT PUSH"
	exit 1
fi
success "PUSHED"
